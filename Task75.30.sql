-- 1 Hiển thị toàn bộ thông tin của tất cả môn học
SELECT * FROM `subjects`;

-- 2 Hiển thị mã môn, tên môn, và số tín chỉ của 3 sinh viên
SELECT subjects.subject_name,subjects.subject_code,subjects.credit FROM subjects WHERE subjects.id IN(1,2,3);

-- 3 Hiển thị toàn bộ thông tin 4 môn có số tín chỉ thấp nhất
SELECT *FROM subjects ORDER BY subjects.credit DESC LIMIT 4;

-- 4 Hiển thị thông tin của sinh viên có mã sinh viên là: 20101234
SELECT * FROM students WHERE students.student_code="20101234";

-- 5 Hiển thị toàn bộ danh sách điểm của môn MAT101 Các cột: Mã môn, tên môn, tên sinh viên, điểm thi, ngày thi
SELECT subjects.subject_code,subjects.subject_name,students.user_name,grades.grade,grades.exam_date 
    FROM grades INNER JOIN subjects ON grades.subject_id=subjects.id 
    INNER JOIN students ON grades.student_id=students.id;

-- 6 Lấy ra danh sách các môn có ngày thi là ngày 2021-05-04
SELECT subjects.subject_name, grades.exam_date 
    FROM grades INNER JOIN subjects ON grades.subject_id=subjects.id WHERE grades.exam_date="2021-05-04";

-- 7 Lấy ra thông tin 3 điểm thi thấp nhất: môn học, tên sinh viên, điểm thi, ngày thi
SELECT subjects.subject_name,students.user_name ,grades.grade ,grades.exam_date 
    FROM grades INNER JOIN subjects ON grades.subject_id=subjects.id 
    INNER JOIN students ON grades.student_id=students.id ORDER BY grades.grade LIMIT 3;
-- 8 Lấy ra thông tin 1 sinh viên có điểm cao nhất của môn ECO101 Nếu có nhiều sinh viên có điểm bằng nhau thì vẫn lấy 1 sinh viên
SELECT students.student_code,students.user_name,students.first_name,students.last_name,students.create_date,students.update_date,grades.grade,subjects.subject_code 
    FROM grades INNER JOIN students ON grades.student_id=students.id 
    INNER JOIN subjects ON grades.subject_id=subjects.id 
    WHERE subjects.subject_code="ECO101" ORDER BY grades.grade DESC LIMIT 1;
